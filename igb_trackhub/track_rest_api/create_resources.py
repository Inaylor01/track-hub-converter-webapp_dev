import requests
import json
from enum import Enum
from xml.dom import minidom
import re
from collections import defaultdict
class Data_Type(Enum):
    CONTENTS = 1
    TRACKDB = 2
class Create_Resources():
    def __init__(self):
        self.trackHubToIGB = {}
        self.IGBToTrackHub = defaultdict(list)
        url = "https://bitbucket.org/lorainelab/integrated-genome-browser/raw/5c846130e207055cb09de3b00a57a58559800759/core/synonym-lookup/src/main/resources/synonyms.txt"
        res = requests.get(url)
        if res:
            lines = res.text.split("\n")
            for line in lines:
                val = line.split("\t")
                IGBName = val[0]
                for x in range(1, len(val)):
                    self.trackHubToIGB[val[x]] = IGBName
                    self.IGBToTrackHub[IGBName].append(val[x])




    def get_hubGenomes_txt(self, hub_link, data_type, genomes = None):
        url = "https://api.genome.ucsc.edu/list/hubGenomes?hubUrl="+hub_link
        res = requests.get(url)
        result = ""
        if res:
            res_json = json.loads(res.text)
            res_genomes = res_json["genomes"]
            for x in res_genomes:
                if data_type == Data_Type.CONTENTS:
                    if x in self.trackHubToIGB:
                        genome = self.trackHubToIGB[x]
                    else:
                        genome = x
                    result += str(genome) +"\t"+ str(res_genomes[x]['description'])+"\n"
                elif data_type == Data_Type.TRACKDB and x in genomes:
                    return res_genomes[x]['trackDbFile'], res_genomes[x]['twoBitPath']
        else:
            result = "404 error"
        return result

    def get_track_db_loc(self, hub_link, genome):
        genome_names = []
        if genome in self.IGBToTrackHub:
            for x in self.IGBToTrackHub[genome]:
                genome_names.append(x)
        else:
            genome_names.append(genome)

        return self.get_hubGenomes_txt(hub_link, Data_Type.TRACKDB, genome_names)

    # [track] / [type] / [short label]
    def create_annots_xml(self, hub_link, genome_name):
        track_db_loc, two_bit_file = self.get_track_db_loc(hub_link, genome_name)
        full_path = '/'.join(track_db_loc.split('/')[:-1]) + "/"
        res = requests.get(track_db_loc)
        if res:
            con = res.text.split(" ")
            if con[0] == "include":
                url =  full_path+con[1]
                url = url.strip('\n')
                res = requests.get(url)
        result = ""
        if res:
            content = res.text.split("\n")
            # create file
            root = minidom.Document()
            # creat root element
            xml = root.createElement('files')
            root.appendChild(xml)

            if two_bit_file:
                filesChild = root.createElement('file')
                filesChild.setAttribute('name', two_bit_file)
                filesChild.setAttribute('reference', "true")
                xml.appendChild(filesChild)
            trackName = bigDataUrl = type = shortLabel = description = ""
            for line in content:
                line = re.sub(' +', ' ', line)
                line = line.strip()
                if line != "":
                    label, content = line.split(" ", 1)
                if label == "track" :
                    trackName = content
                elif label == "shortLabel":
                    shortLabel = content
                elif label == "bigDataUrl":
                    bigDataUrl = content
                    bigDataUrl = bigDataUrl if "http" in bigDataUrl else full_path+bigDataUrl
                elif label == "type":
                    type = content
                elif label == "longLabel":
                    description = content
                # create child element
                if line == "" and bigDataUrl != "":
                    filesChild = root.createElement('file')
                    filesChild.setAttribute('name', bigDataUrl)
                    filesChild.setAttribute('title', (trackName+"/"+type+"/"+shortLabel).replace(" ", "_"))
                    if description:
                        filesChild.setAttribute('description', description)
                        description = ""
                    xml.appendChild(filesChild)
                    trackName = ""
                    bigDataUrl = ""
                    type = ""
                    shortLabel = ""
            xml_str = root.toprettyxml(indent ="\t")

            return xml_str
        return "404 error"

    def create_contents_txt(self, hub_link):
        return self.get_hubGenomes_txt(hub_link, Data_Type.CONTENTS)


    def create_genome_txt(self, hub_link, genome, assembly):
        genome_names = []
        if genome in self.IGBToTrackHub:
            for x in self.IGBToTrackHub[genome]:
                genome_names.append(x)
        else:
            genome_names.append(genome)
        result = ""
        for val in genome_names:
            url = "https://api.genome.ucsc.edu/list/chromosomes?hubUrl="+hub_link+";genome={};track={}".format(val, assembly)
            res = requests.get(url)
            result = ""
            if res:
                res_json = json.loads(res.text)
                res_genomes = res_json["chromosomes"]

                for x in res_genomes:
                    result += str(x) +"\t"+ str(res_genomes[x])+"\n"
                break
        return result

if __name__ == '__main__':
    cr = Create_Resources()
    contents = cr.create_contents_txt("http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/hub.txt")
    chromosomes = cr.create_genome_txt("http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/hub.txt", "CAST_EiJ", "assembly")
    annots = cr.create_annots_xml("http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/hub.txt", "CAST_EiJ")
    #print(contents)
    #print("---------------------------------------------")
    #print("---------------------------------------------")
    #print(chromosomes)
    #print("---------------------------------------------")
    #print("---------------------------------------------")
    #print(annots)
