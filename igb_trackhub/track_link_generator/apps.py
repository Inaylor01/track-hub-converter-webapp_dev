from django.apps import AppConfig


class TrackLinkGeneratorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'track_link_generator'
