from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='th-main'),
    path('returnlink/', views.link, name='th-returnlink')
]