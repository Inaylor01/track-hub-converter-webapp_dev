from django.shortcuts import render


def home(request):
    return render(request, 'track_link_generator/mainscreen.html')
# Create your views here.

#view to return the returnlink page for the user.
def link(request):

    #return render(request, 'trackhub/returnlink.html')
    data= request.POST.get('th_link_textbox')
    
    #Call validation function here, perhaps?

    # Logic for the actual linkmaking goes here
    newLink = request.get_host() + '?trackhub=' + data

    #created context dictionary and pass in data from user to the template file
    context={'newlink':newLink}
    return render(request, 'track_link_generator/returnlink.html', context)
